#!/bin/bash 

## Before running this script you have to run : gcloud auth login to grant access

## Source environment variables
. ./gcp_project.ini

## Create the service account
gcloud iam service-accounts create ${SA_NAME} --display-name "My ${PROJECT_NAME} project Service Account"

## Grant permissions to the service account
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member "serviceAccount:${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" --role "roles/${SA_ROLE}"

## Generate the key file : json file
gcloud iam service-accounts keys create ${KEY_FILE_NAME} --iam-account ${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

## Provide authentication credentials
export GOOGLE_APPLICATION_CREDENTIALS="${KEY_FILE_PATH}/${KEY_FILE_NAME}"

export GOOGLE_CLOUD_KEYFILE_JSON=$(cat <your_json_key_file>)