resource "google_compute_instance" "cb-cluster-01" {
  name         = "cb-cluster-01"
  machine_type = "n1-standard-1"

  tags = ["allow-ssh,allow-http"]
  // metadata_startup_script = "${file("./install-nginx.sh")}"
  
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  zone = "${var.zone}"
  network_interface {
    subnetwork = "cb-cluster-tech-subnet"
    access_config {
      // Ephemeral IP : The Ip address will be available until the instance is alive, when the stack is destroy and apply again, the public Ip is a new one
    }
  }
}

resource "google_compute_instance" "cb-cluster-02" {
  name         = "cb-cluster-02"
  machine_type = "n1-standard-1"

  tags = ["allow-ssh,allow-http"]
  // metadata_startup_script = "${file("./install-nginx.sh")}"
  
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  zone = "${var.zone}"
  network_interface {
    subnetwork = "cb-cluster-tech-subnet"
    access_config {
      // Ephemeral IP : The Ip address will be available until the instance is alive, when the stack is destroy and apply again, the public Ip is a new one
    }
  }
}

resource "google_compute_instance" "cb-cluster-03" {
  name         = "cb-cluster-03"
  machine_type = "n1-standard-1"

  tags = ["allow-ssh,allow-http"]
  // metadata_startup_script = "${file("./install-nginx.sh")}"
  
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  zone = "${var.zone}"
  network_interface {
    subnetwork = "cb-cluster-tech-subnet"
    access_config {
      // Ephemeral IP : The Ip address will be available until the instance is alive, when the stack is destroy and apply again, the public Ip is a new one
    }
  }
}