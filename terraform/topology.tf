# providers info
provider "google" {
  credentials = "${file("../couchbasecluster_cred.json")}"
  project     = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}