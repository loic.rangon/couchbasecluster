Infra as Code pour le Projet GCP de la RFI
==========================================

Objectif
--------

Créer toute l'infrastructure "As Code" pour le projet GCP de la RFI.

Notamment:

1. créer le VPC
2. créer le cluster GKE
3. créer les instances

Partie réseau
-------------

* Un réseau (qui sera apairé avec les GKE master) avec quatre sous-réseaux
1. subnet-admin avec tous les accès sur tout le réseau
2. subnet-editor avec les accès au Master du GKE "editor" et certaines IP services du GKE.
3. subnet-editor-cluster qui soutient uniquement les noeuds du cluser pour les éditeurs.
4. subnet-tech-cluster qui soutient uniquement les noeuds du cluster pour les administrateurs.
* Un routeur vers la gateway NAT avec une IP fixe sortante.
* Un DNS privée/publique avec entrée inverse (PTR)

Partie Cluster
--------------

1. Un Cluster pour les éditeurs afin de déployer leurs gateways, etc.
2. Un Cluster pour les administrateurs afin de déployer leurs services (injecteurs de charge, etc.)

* Activer et Configurer le RBAC
* Activer et Configurer les PSP
* Activer et Configurer les NetworkPolicies
* Définir les resourceQuota et limitRange
* Installer deux Nginx Ingress Controler: un pour l'exposition publique et un autre pour l'exposition privée
* Autoriser les LoadBalancer pour les Cluster
* Mettre en place Prometheus Operator à l'identique du Cluster Azure

Partie instances
----------------

* VM éditeurs
* serveur SMTP ou relais SMTP
* serveur pour les tests de charge
* serveur bastion
* serveur centreon


N.B.: le service ssh de GCP offre la possibilité d'avoir recours au sous-processus sftp pour le trasnfert des fichiers. Il est prévu que chaque éditeur utilise sa machine d'administration pour les transferts sftp.
***
maintainer: API Team