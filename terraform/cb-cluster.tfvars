/*  Global variables
    project
    region
    zone
    label
*/


name = "couchbasecluster"
project = "couchbasecluster-230608"
region = "europe-west1"
zone = "europe-west1-b"

label = "lab"

/* network variables
   name
*/

network = {
    name = "cb-cluster-network"
}


/* subnet variables
   four subnet are declared within this poc
*/

cb-cluster-tech-subnet = {
    name = "tech-subnet"
    ip_cidr_range = "192.168.122.0/24"
}


/* firewall rules
   ssh from internet
   all ports from administration subnet
*/

ssh-rule = "allowssh"
http-rule= "allowhttp"
admin-rule = "fulladmin"