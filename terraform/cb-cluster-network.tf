/*  General resources
    project metadata
    network
*/

// Generate global authorized keys over the project instances
/*
resource "google_compute_project_metadata" "adminers" {
  metadata {
    ssh-keys  = "user-name:${file("path-to-public-key")}"
  }
}
*/

// Create the sole VPC network to be peered to the clusters
resource "google_compute_network" "vpc-network" {
  name                    = "${var.name}-${var.network["name"]}"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "cb-cluster-tech-subnet" {
  name          = "${var.name}-${var.cb-cluster-tech-subnet["name"]}"
  ip_cidr_range = "${var.cb-cluster-tech-subnet["ip_cidr_range"]}"
  region        = "${var.region}"
  network       = "${google_compute_network.vpc-network.self_link}"

  private_ip_google_access = true

}

resource "google_compute_firewall" "allow-http" {
  name = "allow-http"
  network = "${google_compute_network.vpc-network.name}"
  target_tags = ["allow-http"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports = ["80"]
  }
}

resource "google_compute_firewall" "allow-https" {
  name = "allow-https"
  network = "${google_compute_network.vpc-network.name}"
  target_tags = ["allow-https"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports = ["443"]
  }
}

resource "google_compute_firewall" "allow-ssh" {
  name = "allow-ssh"
  network = "${google_compute_network.vpc-network.name}"
  target_tags = ["allow-ssh"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports = ["22"]
  }
}

